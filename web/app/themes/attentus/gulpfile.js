/** <IMPORTANT> */
let siteName = 'attentus-wp'; // IMPORTANT: Set your local HOSTNAME name without TLD here
/** </IMPORTANT> */

// Require our dependencies.
let browserSync = require('browser-sync').create();
let gulp = require('gulp');

// Set assets paths.
let paths = {
	php: [
		'*.php',
		'**/*.php'
	],
	scripts: [
		'js/*.js'
	],
	styles: [
		'*.php',
		'styles/css/*.css',
		'images/*.{png,jpg,gif,svg}',
		'pages/**/*.php',
		'pages/**/*.twig',
		'pages/**/*.scss',
		'views/*.twig',
		'views/**/*.twig'
	]
};

/**
 * Reload browser after PHP & JS file changes and inject CSS changes.
 *
 * https://browsersync.io/docs/gulp
 */
gulp.task('default', function () {
	browserSync.init({
		proxy: 'http://www.' + siteName + '.test',
		host: siteName + '.test',
		open: 'external',
		port: 8000

		// Uncomment the lines below if working with SSL
		/*https: {
			key:
				'/Users/' +
				userName +
				'/.valet/Certificates/' +
				siteName +
				'.test.key',
			cert:
				'/Users/' +
				userName +
				'/.valet/Certificates/' +
				siteName +
				'.test.crt'
		}*/
	});

	gulp.watch(paths.php).on('change', browserSync.reload);
	gulp.watch(paths.scripts).on('change', browserSync.reload);
	gulp.watch(paths.styles).on('change', browserSync.reload);

	gulp.watch(paths.styles, function () {
		gulp.src(paths.styles).pipe(browserSync.stream());
	});
});