msgid ""
msgstr ""
"Project-Id-Version: attentus\n"
"POT-Creation-Date: 2021-09-11 22:49+0200\n"
"PO-Revision-Date: 2021-09-11 22:50+0200\n"
"Last-Translator: Kolja Nolte <kolja.nolte@gmail.com>\n"
"Language-Team: Kolja Nolte <kolja.nolte@gmail.com>\n"
"Language: de_DE\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.4.3\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-Flags-xgettext: --add-comments=translators:\n"
"X-Poedit-WPHeader: style.css\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;"
"_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.min.js\n"

#: classes/Site.php:83
msgid "Menu in the header"
msgstr "Menü im Header"

#: classes/Site.php:88
msgid "Menu in the footer"
msgstr "Menü im Footer"

#: classes/Site.php:137
msgctxt "Date of user registration"
msgid "Registered"
msgstr "Registrieert"

#: classes/Site.php:163
msgid "Clear Timber cache"
msgstr "Timber-Cache leeren"

#: classes/Site.php:176
msgid "Quick Action"
msgstr "Schnellaktion"

#. Theme Name of the plugin/theme
#: classes/Site.php:245 classes/Site.php:246
msgid "attentus WP"
msgstr "attentus WP"

#: functions.php:42 functions.php:81
msgid "Search "
msgstr "Suche "

#: functions.php:43 functions.php:80
msgid "All "
msgstr "Alle "

#: functions.php:44 functions.php:45 functions.php:82
msgid "Parent "
msgstr "Übergeordnet "

#: functions.php:46 functions.php:78
msgid "Edit "
msgstr "Bearbeiten "

#: functions.php:47
msgid "Update "
msgstr "Aktualisieren "

#: functions.php:48 functions.php:76
msgid "Add New "
msgstr "Neu hinzufügen "

#: functions.php:49 functions.php:77
msgid "New "
msgstr "Neu "

#: functions.php:75
msgid "Add New"
msgstr "Neu hinzufügen"

#: functions.php:79
msgid "View "
msgstr "Ansehen "

#: functions.php:83 functions.php:85
msgid "No "
msgstr "Nein "

#: functions.php:89
msgctxt ""
"Overrides the \"Featured Image\" phrase for this post type. Added in 4.3"
msgid " cover image"
msgstr " Titelbild"

#: functions.php:94
msgctxt ""
"Overrides the \"Set featured image\" phrase for this post type. Added in 4.3"
msgid "Set cover image"
msgstr "Titelbild festlegen"

#: functions.php:99
msgctxt ""
"Overrides the \"Remove featured image\" phrase for this post type. Added in "
"4.3"
msgid "Remove cover image"
msgstr "Cover-Bild entfernen"

#: functions.php:104
msgctxt ""
"Overrides the \"Use as featured image\" phrase for this post type. Added in "
"4.3"
msgid "Use as cover image"
msgstr "Als Titelbild verwenden"

#: functions.php:109
msgctxt ""
"The post type archive label used in nav menus. Default \"Post Archives\". "
"Added in 4.4"
msgid " archives"
msgstr " Archiv"

#: functions.php:114
msgctxt ""
"Overrides the \"Insert into post\"/\"Insert into page\" phrase (used when "
"inserting media into a post). Added in 4.4"
msgid "Insert into "
msgstr "Einfügen in "

#: functions.php:119
msgctxt ""
"Overrides the \"Uploaded to this post\"/\"Uploaded to this page\" phrase "
"(used when viewing media attached to a post). Added in 4.4"
msgid "Uploaded to this "
msgstr "Hochgeladen zu "

#: functions.php:124
msgctxt ""
"Screen reader text for the filter links heading on the post type listing "
"screen. Default \"Filter posts list\"/\"Filter pages list\". Added in 4.4"
msgid "Filter "
msgstr "Filter "

#: functions.php:129
msgctxt ""
"Screen reader text for the pagination heading on the post type listing "
"screen. Default \"Posts list navigation\"/\"Pages list navigation\". Added "
"in 4.4"
msgid " list navigation"
msgstr " listennavigation"

#: functions.php:134
msgctxt ""
"Screen reader text for the items list heading on the post type listing "
"screen. Default \"Posts list\"/\"Pages list\". Added in 4.4"
msgid " list"
msgstr " Liste"

#: init.php:72
#, php-format
msgid "ERROR: Could not load plugin %s"
msgstr "FEHLER: Plugin %s konnte nicht geladen werden"

#: views/base.twig:21
msgid "Toggle navigation"
msgstr "Navigation umschalten"

#. Theme URI of the plugin/theme
msgid "https://www.attentus.com/attentus-wp/"
msgstr "https://www.attentus.com/attentus-wp/"

#. Description of the plugin/theme
msgid ""
"attentus WP a Bootstrap 5-based starter theme for the boilerplate attentus "
"WP with support for Twig, Timber, environments, PHP Composer, and more."
msgstr ""
"attentus WP ist ein Boilerplate basierend auf Bootstrap 5 mit Unterstützung "
"für Twig Template Engine, Timber, Umgebungsvariablen, Paketmanager mit PHP "
"Composer und mehr."

#. Author of the plugin/theme
msgid "attentus / Kolja Nolte"
msgstr "attentus / Kolja Nolte"

#. Author URI of the plugin/theme
msgid "mailto:nolte@attentus.com"
msgstr "mailto:nolte@attentus.com"

#~ msgid "attentus"
#~ msgstr "attentus"

#~ msgid ""
#~ "Ein WordPress Boilerplate basierend auf Bedrock mit PHP Composer und mit "
#~ "Twig-Unterstützung durch Timber"
#~ msgstr ""
#~ "Ein WordPress Boilerplate basierend auf Bedrock mit PHP Composer und mit "
#~ "Twig-Unterstützung durch Timber"
